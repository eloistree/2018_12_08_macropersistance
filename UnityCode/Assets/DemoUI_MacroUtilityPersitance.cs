﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DemoUI_MacroUtilityPersitance : MonoBehaviour
{
    [Header("Lines Editing")]
    public InputField m_quickMacroEditing;
    public TextAsset m_linesEditingExample;

    [Header("XML Editing")]
    public InputField m_macroName;
    public InputField m_xmlMacroEditing;

    [Header("Debug Text")]
    public Text m_debugText;

    [Header("Macro")]
    public Button m_playButton;
    public TimedMacro m_macro;



    private void Awake()
    {
        MacroInputMono.CreateSceneInstance();
    }



    public void ConvertQuickEditingToXml() {
        TimedMacro macro = TextToMacro.ConvertLinesMacroToTimeMacro(m_quickMacroEditing.text);
        macro.SetName(m_macroName.text);
        string xml = TextToMacro.ConvertMacroToXML(macro);
        m_xmlMacroEditing.text = xml;
        m_macro = macro;
        RefreshMacroLoaded();
    }

    public void SetDefaultExample() {
        if (m_linesEditingExample != null)
            m_quickMacroEditing.text = m_linesEditingExample.text;
    }


    public void Save()
    {
        TimedMacro macro = TextToMacro.ConvertXmlToTimedMacro(m_xmlMacroEditing.text);
        m_macro = macro;
        macro.SetName(m_macroName.text);
        MacroInput.Save<TimedMacro>(macro);
    }
    public void Load()
    {
       TimedMacro macro = MacroInput.Load<TimedMacro>(m_macroName.text);
        if (macro != null)
        {
            m_macro = macro;
            m_xmlMacroEditing.text = TextToMacro.ConvertMacroToXML(macro);
            RefreshMacroLoaded();
        }
        else {
            m_debugText.text = "Load macro failed";
        }

    }

    public void Play()
    {
        MacroInput.PlayMacro(m_macroName.text);
    }

    public void ResetAll()
    {
        m_macroName .text= "DefaultMacro";
        m_quickMacroEditing.text = "";
        m_xmlMacroEditing.text = "";
        m_macro=null;
    }

    public void RefreshMacroLoaded() {

        m_playButton.interactable = m_macro != null && ! string.IsNullOrEmpty( m_macro.m_name);
    }
}
