﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoExectureMacroActionInUnity : MonoBehaviour
{
    public  void ExectureAction(A_Action action)
    {
        Debug.Log(action.GetOneLineDescription());


        if (action.GetType() == typeof(OpenBrowserAction)) {
            OpenBrowserAction castedAction = (OpenBrowserAction)action;
            Application.OpenURL(castedAction.m_url);
        }
    }

}
