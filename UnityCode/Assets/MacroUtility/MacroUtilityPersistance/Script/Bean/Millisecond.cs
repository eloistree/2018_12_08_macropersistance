﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Millisecond 
{

    private int m_value;

    public Millisecond(int value)
    {
        Value = value;
    }

    public int Value
    {
        get { return m_value; }
        set {
            if (value < 0)
                m_value = 0;
            else 
                m_value = value; }
    }


}
