﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


[System.Serializable]
public class MouseStrokeAction : A_Action
{

    [XmlAttribute("MouseIndex")]
    public int m_mouseIndex;
    [XmlAttribute("PressionType")]
    public PressionType m_pressionType;

    public void SetWith(MouseClickType clickType) { }
    public override string GetOneLineDescription()
    {
        return "Mouse Click | " + m_mouseIndex +" "+ (m_pressionType == PressionType.Down ? '↓' : '↑');
    }


}

public enum MouseClickType : int { Left, Middle, Right, Alt1, Alt2, Alt3 }