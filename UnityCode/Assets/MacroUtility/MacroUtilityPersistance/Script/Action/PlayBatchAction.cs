﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


    [System.Serializable]
public class PlayBatchAction :  A_Action
 {
        [XmlAttribute("AudioName")]
        public string m_batchName;
        public override string GetOneLineDescription()
        {
            return "Play Batch | " + m_batchName;
        }
  
    
}
