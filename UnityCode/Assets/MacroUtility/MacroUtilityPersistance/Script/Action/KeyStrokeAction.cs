﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


[System.Serializable]
public class KeyStrokeAction : A_Action
{
    [XmlAttribute("Key")]
    public string m_keyEnumeration;
    [XmlAttribute("PressionType")]
    public PressionType m_pressionType;

    public override string GetOneLineDescription()
    {
        return "Stroke Key | " + m_keyEnumeration + " " + (m_pressionType==PressionType.Down? '↓' : '↑');
    }


}

public enum PressionType { Down, Up }