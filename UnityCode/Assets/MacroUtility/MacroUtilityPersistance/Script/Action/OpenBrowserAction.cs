﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


[System.Serializable]
public class OpenBrowserAction : A_Action
{
    [XmlAttribute("Url")]
    public string m_url;
    [XmlAttribute("BrowserName")]
    public string m_browserName;

    public override string GetOneLineDescription()
    {
        return "Open Browser | " + m_url;
    }
    
}
