﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


[System.Serializable]
public class PlaySoundAction : A_Action
{
    [XmlAttribute("AudioName")]
    public string m_audioFileName;
    [XmlAttribute("PlayType")]
    public PlayType m_audioPlayTime;
    [XmlAttribute("Volume")]
    public float m_volume=1;

    public override string GetOneLineDescription()
    {
        return "Play sound | " + m_audioFileName;
    }
    public enum PlayType {
        Main, Parallax
    }
}
