﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


[System.Serializable]
public class OpenFileAction : A_Action
{
    [XmlAttribute("FilePath")]
    public string m_filePath;
    [XmlAttribute("PathType")]
    public string m_pathType = PathType.Relative.ToString();

    public override string GetOneLineDescription()
    {
        return "Open File | " + m_filePath;
    }

    public enum PathType { Relative, Aboslut}
}
