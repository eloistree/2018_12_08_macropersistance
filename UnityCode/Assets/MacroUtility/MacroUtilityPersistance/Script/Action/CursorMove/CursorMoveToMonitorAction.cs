﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[System.Serializable]
public class CursorMoveToMonitorAction : A_Action
{
    [XmlAttribute("MonitorIndex")]
    public int m_monitorIndex;
   public Pourcent2D m_position;


}



[System.Serializable]
public class Pourcent2D
{

    [XmlAttribute("X")]
    public float m_x = 0.5f;
    [XmlAttribute("Y")]
    public float m_y = 0.5f;
}