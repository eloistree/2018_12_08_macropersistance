﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

[System.Serializable]
public class CursorMoveToZoneAction : A_Action
{
    [XmlAttribute("ZoneName")]
    public string m_zoneName;
    public Pourcent2D m_position;


}