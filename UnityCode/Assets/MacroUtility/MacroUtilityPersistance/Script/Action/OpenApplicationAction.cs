﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


[System.Serializable]
public class OpenApplicationAction : A_Action
{
    [XmlAttribute("ApplicationName")]
    public string m_applicationName;

    public override string GetOneLineDescription()
    {
        return "Open Application | " + m_applicationName;
    }
    
}
