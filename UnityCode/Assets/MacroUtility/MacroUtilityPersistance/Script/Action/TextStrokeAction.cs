﻿using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class TextStrokeAction : A_Action
{
    public string m_textToWrite;

    public override string GetOneLineDescription() {
        return "Write Text | " + m_textToWrite;
    }


}
