﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


[System.Serializable]

[XmlInclude(typeof(TextStrokeAction))]
[XmlInclude(typeof(KeyStrokeAction))]
[XmlInclude(typeof(PlaySoundAction))]
[XmlInclude(typeof(PlayBatchAction))]
[XmlInclude(typeof(CursorMoveToMonitorAction))]
[XmlInclude(typeof(CursorMoveToZoneAction))]
[XmlInclude(typeof(MouseStrokeAction))]
[XmlInclude(typeof(OpenBrowserAction))]
[XmlInclude(typeof(OpenApplicationAction))]
[XmlInclude(typeof(OpenFileAction))]


public class A_Action : I_Action
{
    public virtual string GetOneLineDescription()
    {
        return this.ToString();
    }
}
