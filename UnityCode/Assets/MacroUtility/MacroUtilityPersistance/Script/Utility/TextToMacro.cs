﻿using System;
using System.Collections;
using System.Collections.Generic;


public class TextToMacro 
{

    public static TimedMacro ConvertLinesMacroToTimeMacro(string text) {
        TimedMacro macro = new TimedMacro();
        TryToConvertFollowingLines(text, ref macro);
        return macro;

    }

    public static TimedMacro ConvertXmlToTimedMacro(string xmlMacro) {

        TimedMacro macro = Macro.XmlDeserializeFromString<TimedMacro>(xmlMacro);
        macro.SortByTime();
        return macro;
    }





        #region LINES CONVERTER


        public static void TryToConvertFollowingLines(string text, ref TimedMacro macro)
        {


            string[] token = text.Split('\n');
            for (int i = 0; i < token.Length; i++)
            {
                TryToConvertFollowingLine(token[i], ref macro);
            }
        }

    internal static string ConvertMacroToXML(TimedMacro macro)
    {
        return Macro.XmlSerializeToString<TimedMacro>(macro);
    }

    public static void TryToConvertFollowingLine(string line, ref TimedMacro macro)
        {

            string[] token = line.Split('|');

            if (token.Length == 3 && token[0].Trim() == "Play Sound")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new PlaySoundAction() { m_audioFileName = token[2].Trim() } );
            }
            if (token.Length == 3 && token[0].Trim() == "Play Batch")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new PlayBatchAction() { m_batchName = token[2].Trim() } );
            }
            if (token.Length == 3 && token[0].Trim() == "Stoke Text")
            {
                macro.AddAction(  int.Parse(token[1].Trim()), new TextStrokeAction() { m_textToWrite = token[2] });
            }
            if (token.Length == 4 && token[0].Trim() == "Stoke Touch")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new KeyStrokeAction()
                    {
                        m_keyEnumeration = token[2].Trim(),
                        m_pressionType = token[3].ToLower() == "down" ? PressionType.Down : PressionType.Up
                    }
                );
            }
            if (token.Length == 3 && token[0].Trim() == "Open Browser")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new OpenBrowserAction()
                    {
                        m_url = token[2].Trim(),
                        m_browserName = "Unkown"
                    }
                );
            }
            if (token.Length == 3 && token[0].Trim() == "Open Application")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new OpenApplicationAction()
                    {
                        m_applicationName = token[2].Trim()
                    }
                );
            }

            if (token.Length == 4 && token[0].Trim() == "Open File")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new OpenFileAction()
                    {
                        m_pathType = token[2].Trim(),
                        m_filePath = token[3].Trim()
                    }
                );
            }

            if (token.Length == 4 && token[0].Trim() == "Mouse Click")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new MouseStrokeAction()
                    {
                        m_mouseIndex = int.Parse(token[2]),
                        m_pressionType = token[3].ToLower() == "down" ? PressionType.Down : PressionType.Up
                    }
               );
            }
            if (token.Length == 5 && token[0].Trim() == "Mouse Move Monitor")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new CursorMoveToMonitorAction()
                    {
                        m_monitorIndex = int.Parse(token[2]),
                        m_position = new Pourcent2D()
                        {
                            m_x = float.Parse(token[3]),
                            m_y = float.Parse(token[4])
                        }
                    }
                );
            }
            if (token.Length == 5 && token[0].Trim() == "Mouse Move Zone")
            {
                macro.AddAction(int.Parse(token[1].Trim()), new CursorMoveToZoneAction()
                    {
                        m_zoneName = token[2],
                        m_position = new Pourcent2D()
                        {
                            m_x = float.Parse(token[3]),
                            m_y = float.Parse(token[4])
                        }
                    }
                );
            }


            #endregion



        }

    }

