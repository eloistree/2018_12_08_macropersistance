﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

public interface I_Macro {
    string GetName();
    void SetName(string name);
    string GetXmlEditableVersion();
    T GetMacro<T>(string xml) where T : Macro;
}

[XmlRoot("Macro")]
[System.Serializable]
public class Macro : I_Macro
{
    
    public  string m_name;


    
    public string GetName()
    {
        return m_name;
    }

    public void SetName(string name)
    {
        m_name = name;
    }


    public string GetXmlEditableVersion()
    {

        Macro macro = new Macro();
        return XmlSerializeToString(this);
    }



    public  T GetMacro<T>(string xml) where T:Macro
    {
        T settings = XmlDeserializeFromString<T>(xml);
        return settings;

    }
    #region XML SERIALIZE
    public static string XmlSerializeToString<T>( T macro) where T:I_Macro
    {
        XmlSerializer serializer = new XmlSerializer(macro.GetType());
        StringBuilder sb = new StringBuilder();   
        //////dddddd
        using (TextWriter writer = new StringWriter(sb))
        {

            serializer.Serialize(writer, macro);
        }
       
        return sb.ToString();
    }

    public static T XmlDeserializeFromString<T>( string objectData)
    {
        return (T)XmlDeserializeFromString(objectData, typeof(T));
    }

    public static object XmlDeserializeFromString( string objectData, Type type)
    {
        var serializer = new XmlSerializer(type);
        object result;

        using (TextReader reader = new StringReader(objectData))
        {
            result = serializer.Deserialize(reader);
        }

        return result;
    }
    #endregion
}

[XmlRoot("TimedMacro")]
[System.Serializable]
public class TimedMacro :Macro
{

    

    [XmlArray("ActionFrames")]
    [XmlArrayItem("ActionFrame")]
    public TimeToAction[] m_timeToAction = new TimeToAction[] {
    };

    

    public void AddAction(int millisecond, A_Action action) {
        TimeToAction[] newTable = new TimeToAction[m_timeToAction.Length + 1];
        for (int i = 0; i < m_timeToAction.Length; i++)
        {
            newTable[i] = m_timeToAction[i];
        }
        newTable[m_timeToAction.Length] = new TimeToAction() { m_millisecond = millisecond, m_action = action };
        m_timeToAction = newTable;
    }


    public void SortByTime()
    {
        m_timeToAction = m_timeToAction.OrderBy(k => k.m_millisecond).ToArray();
    }


    internal List<A_Action> GetActionsSortBetween(float minMilliseconds , float maxMilliseconds)
    {
        if (m_timeToAction.Length == 0)
            return  new List<A_Action>();

        List<A_Action> actions = m_timeToAction
            .Where(k => k.m_millisecond >= minMilliseconds && k.m_millisecond <= maxMilliseconds)
            .OrderBy(k => k.m_millisecond).Select(k=>k.m_action).ToList();
        return actions;
    }


    internal float GetDuration()
    {
        if (m_timeToAction.Length == 0)
            return 0f;
        return m_timeToAction.OrderByDescending(k => k.m_millisecond).First().m_millisecond;
    }
}
[System.Serializable]
public struct TimeToAction {



    [XmlAttribute("Millisecond")]
    public int m_millisecond;
    public A_Action m_action;

}