﻿using System.Collections;
using System.Collections.Generic;
using System;

public class MacroInput 
{
    private static  I_MacroManager m_macroManager = new MacroManagerDefault();
    public static OnActionRequested onActionRequested;

    public static void PlayMacro(string name) {
        m_macroManager.Play(name);
    }

    internal static void Save<T>(T macro) where T: I_Macro
    {
        m_macroManager.Save<T>(macro);
    }


    internal static T Load<T>(string name) where T: I_Macro
    {
        return m_macroManager.Load<T>(name);
    }

    public  static void SetMacroManager(I_MacroManager macroManager) {
        m_macroManager = macroManager;
    }
    
}


public delegate void OnActionRequested(A_Action action);