﻿using System.Collections;
using System.Collections.Generic;


public interface I_MacroManager {
    void Play(string name);
    void Save<T>(T macro)where T: I_Macro;
    T Load<T>(string name) where T:I_Macro;
}
