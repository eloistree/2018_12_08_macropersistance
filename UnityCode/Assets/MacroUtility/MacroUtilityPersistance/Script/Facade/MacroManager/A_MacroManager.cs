﻿using System.Collections;
using System.Collections.Generic;


public abstract class A_MacroManager : I_MacroManager
{

    public abstract void Play(string name);
    public abstract void Save<T>(T macro) where T: I_Macro;
    public abstract T Load<T>(string name) where T : I_Macro;
}
