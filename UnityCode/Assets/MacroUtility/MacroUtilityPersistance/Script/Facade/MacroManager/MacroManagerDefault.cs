﻿using System.Collections;
using System.Collections.Generic;
using System.IO;


public class MacroManagerDefault  : A_MacroManager 
{
    public MacroManagerDefault()
    {
        CheckTheFilesAndDirectoriesExist();
    }

    public override void Play(string name)
    {
        CheckTheFilesAndDirectoriesExist();
        throw new System.NotImplementedException("Need to be code by the user of this plug-in");

    }

    public void CheckTheFilesAndDirectoriesExist() {
        CreateReadMeToMacroPlace();
    }

    public  void CreateReadMeToMacroPlace() {
        string directoryPath = GetMacroFolderPath();
        if (! File.Exists(directoryPath+"/ReadMe.txt")) {

            Directory.CreateDirectory(directoryPath);
            File.WriteAllText(directoryPath + "/ReadMe.txt","Hello :)");
        }

    }

    public override void Save<T>(T macro) 
    {
        string macroString = Macro.XmlSerializeToString<T>(macro);

        File.WriteAllText(GetMacroFolderPath() + "/" + macro.GetName()+".xml", macroString);
    }
 


    public string GetMacroFolderPath() {
    return Directory.GetCurrentDirectory() + "/Macro";
    }

    public override T Load<T>(string name)
    {
        string xml = File.ReadAllText(GetMacroFolderPath() + "/" + name + ".xml");

       T  value = Macro.XmlDeserializeFromString<T>(xml);
        return value;
    }
}
