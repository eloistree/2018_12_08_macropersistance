﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMacro : MonoBehaviour
{
    public TimedMacro m_helloWorld;
    public TimedMacro m_dd;

    void Start()
    {
        m_helloWorld.AddAction(100, new TextStrokeAction(){ m_textToWrite = "Hello"} );
        m_helloWorld.AddAction(200, new TextStrokeAction() { m_textToWrite = "World" } );
        m_helloWorld.AddAction(300, new TextStrokeAction() { m_textToWrite = "Salut </io> </br> <io>" } );
        m_helloWorld.SortByTime();

        MacroInput.Save(m_helloWorld);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.L)){
            m_dd = MacroInput.Load<TimedMacro>(m_helloWorld.GetName());
            m_dd.SortByTime();
            for (int i = 0; i < m_dd.m_timeToAction.Length; i++)
            {
                Debug.Log(i + ":" + m_dd.m_timeToAction[i].m_action.GetOneLineDescription());
            }
        }
        
    }
}
