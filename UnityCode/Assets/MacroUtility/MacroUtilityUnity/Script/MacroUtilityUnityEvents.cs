﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MacroActionEvent : UnityEvent<A_Action> { }

[System.Serializable] // Int = Millisecond, 
public class TimedMacroActionEvent : UnityEvent<Millisecond, A_Action> { }

[System.Serializable]
public class TimedMacroEvent : UnityEvent<TimedMacro> { }

[System.Serializable] // Name of the macro
public class MacroToPlayEvent : UnityEvent<string> { }

