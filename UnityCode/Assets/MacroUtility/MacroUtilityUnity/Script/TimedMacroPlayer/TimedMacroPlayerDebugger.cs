﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TimedMacroPlayer))]
public class TimedMacroPlayerDebugger : MonoBehaviour
{
    [SerializeField] TimedMacroPlayer m_target;

    [SerializeField] List<string> m_actionsFound= new List<string>();
    [Range(1,20)]
    [SerializeField] int m_maxDebugLengh=10;
    


    public void Awake()
    {
        if (m_target == null) m_target = GetComponent<TimedMacroPlayer>();
        m_target.m_actionRequested.AddListener(DebugActionFound);
    }

    private void DebugActionFound(Millisecond milliseconds, A_Action action)
    {
        m_actionsFound.Insert(0, milliseconds.Value + "|" + action.GetOneLineDescription());
       while (m_actionsFound.Count > m_maxDebugLengh)
            m_actionsFound.RemoveAt(m_actionsFound.Count-1);
    }

    private void OnValidate()
    {
        if (m_target == null) m_target = GetComponent<TimedMacroPlayer>();
    }
}
