﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedMacroPlayer : MonoBehaviour
{
    [SerializeField] TimedMacro m_timedMacro;
    public bool m_makeMacroLoop;

    [Header("Events")]
    [SerializeField] public TimedMacroEvent m_macroLaunched= new TimedMacroEvent();
    [SerializeField] public TimedMacroActionEvent m_actionRequested = new TimedMacroActionEvent();
    [SerializeField] public TimedMacroEvent m_macroEnd = new TimedMacroEvent();



    [Header("Debug")]
    [SerializeField] bool m_isMacroActive;

    [SerializeField] public float m_macroDuraction;
    [SerializeField] public float m_currentTimer;
    public float m_previousTimer;


    public void SetMacro(TimedMacro macro)
    {
        m_timedMacro = macro;
        m_macroDuraction = m_timedMacro.GetDuration()/1000f;
    }

    public void LaunchMacro() {
        if (m_timedMacro != null)
        {
            ResetTimer();
            m_isMacroActive = true;

        }
        else Debug.LogWarning("Can launch a macro that is null", this.gameObject);
    }

    private void ResetTimer()
    {
        m_currentTimer = 0;
        m_previousTimer = 0;
    }

    public void StopMacro() {
        m_isMacroActive = false;
        ResetTimer();
    }
    
    void Update()
    {
        if (m_isMacroActive)
        {
            m_previousTimer = m_currentTimer;
            m_currentTimer += Time.deltaTime;

            if (m_previousTimer == 0f && m_currentTimer != 0f)
            {
                NotifyMacroLaucnh();
            }

            FoundActionsAndNotifyBetweenFrames(m_previousTimer, m_currentTimer);

            if (m_currentTimer > m_macroDuraction)
            {

                NotifyMacroEnd();
                StopMacro();
                if (m_makeMacroLoop)
                    LaunchMacro();
            }
        }

    }

    private void FoundActionsAndNotifyBetweenFrames(float minseconds, float maxSeconds)
    {
        List<A_Action> actions = m_timedMacro.GetActionsSortBetween(SecondsToMilliseconds(minseconds), SecondsToMilliseconds(maxSeconds) -1);
        if (actions.Count > 0)
        {
            for (int i = 0; i < actions.Count; i++)
            {
                NotifyActionRequested(actions[i]);
            }
        }
    }

    private void NotifyActionRequested(A_Action a_Action)
    {
        m_actionRequested
            .Invoke(new Millisecond
            (SecondsToMilliseconds(m_currentTimer))
            , a_Action);
    }

    private int SecondsToMilliseconds(float seconds)
    {
        return (int)(seconds * 1000f);
    }

    private void NotifyMacroEnd()
    {
        m_macroEnd
            .Invoke(m_timedMacro);
    }

    private void NotifyMacroLaucnh()
    {

        m_macroLaunched
            .Invoke(m_timedMacro);
    }
}
