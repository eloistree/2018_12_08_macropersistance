﻿
using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(QuickMacroInputTrigger))]
public class QuickMacroInputTriggerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        QuickMacroInputTrigger myScript = (QuickMacroInputTrigger)target;
       
        if (Application.isPlaying & GUILayout.Button("Play Macro"))
        {
            MacroInputMono.CreateSceneInstance();
            MacroInput.PlayMacro(myScript.m_macroName);
        }
    }
}