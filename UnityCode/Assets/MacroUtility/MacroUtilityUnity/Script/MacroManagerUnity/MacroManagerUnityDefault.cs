﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MacroManagerUnityDefault : MonoBehaviour
{
    public MacroManagerUnity m_macroManager =  new MacroManagerUnity();

    private void Awake()
    {
        m_macroManager.m_macroToPlay += PlayMacro;
        MacroInput.SetMacroManager(m_macroManager);
    }

    private void PlayMacro(string name)
    {
        Debug.Log("Play this Macro:" + name);
    }
    
}


public class MacroManagerUnity : MacroManagerDefault
{
    public MacroToPlay m_macroToPlay;
    public override void Play(string name)
    {
        if (m_macroToPlay != null)
            m_macroToPlay(name);
    }
}
    public delegate void MacroToPlay(string name);
