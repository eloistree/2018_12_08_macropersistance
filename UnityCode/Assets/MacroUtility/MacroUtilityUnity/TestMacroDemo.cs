﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMacroDemo : MonoBehaviour
{
    public TimedMacroPlayer m_macroPlayer;
    [TextArea(0,40)]
    public string m_text;
    public TextAsset m_fileToTry; 

    void Start()
    {
       TimedMacro importedMacro = new TimedMacro() { m_name="Macro Test Demo"};
       TextToMacro.TryToConvertFollowingLines(m_text,ref  importedMacro);
        m_macroPlayer.SetMacro(importedMacro);
        m_macroPlayer.LaunchMacro();
    }





    private void OnValidate()
    {
        if (m_fileToTry != null) {

           m_text=  m_fileToTry.text;
            m_fileToTry = null;
        }
    }

}
