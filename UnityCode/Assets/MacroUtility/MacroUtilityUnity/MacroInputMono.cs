﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MacroInputMono : MonoBehaviour
{

    public  MacroManagerUnity macroManager;
    private static MacroInputMono m_instance;
    public static MacroInputMono Instance
    {
        private set { m_instance = value; }
        get {
            bool hasBeenInit = false; ;
            if (m_instance == null)
            {
                hasBeenInit = true;
                m_instance = GameObject.FindObjectOfType<MacroInputMono>();
            }
            if (m_instance == null)
            {
                hasBeenInit = true;
                GameObject gamo = Instantiate(Resources.Load("#MacroInputMono") as GameObject);
                gamo.name = "#MacroInputMono";
                m_instance = gamo.GetComponent<MacroInputMono>();
            }

            if (hasBeenInit) {
                m_instance.macroManager = new MacroManagerUnity();
                MacroInput.SetMacroManager(m_instance.macroManager);
                m_instance.ListenTo(m_instance.macroManager);
            }
            return m_instance;
        }
    }

    public  static void CreateSceneInstance()
    {
        CheckForInstance();
    }

    [SerializeField] MacroToPlayEvent m_macroToPlay;
    [SerializeField] MacroActionEvent m_actionDetected;


    private void Awake()
    {
        CheckForInstance();
        MacroInput.onActionRequested += NotifyActionDetected;
    }


    private void OnDestroy()
    {

        MacroInput.onActionRequested -= NotifyActionDetected;
    }

    private void NotifyActionDetected(A_Action action)
    {
        m_actionDetected.Invoke(action);
    }

    private void ListenTo(MacroManagerUnity macroManager)
    {
        macroManager.m_macroToPlay += PlayMacro;

    }

    private void PlayMacro(string name)
    {
        Debug.Log("Play Macro:" + name);

        TimedMacro macro = Load<TimedMacro>(name);
        if (macro!=null) {
            GameObject playerObject = new GameObject("#PlayMacro:" + name);
            TimedMacroPlayer player = playerObject.AddComponent<TimedMacroPlayer>();
            playerObject.AddComponent<TimedMacroPlayerDebugger>();
            player.SetMacro(macro);
            player.LaunchMacro();
            player.m_actionRequested.AddListener( delegate (Millisecond second, A_Action action ) {
            if(MacroInput.onActionRequested!=null)
                MacroInput.onActionRequested(action);
            });
            Destroy(playerObject, player.m_macroDuraction+0.5f);
        }


        // Launch a script or coroutine of 
        // Launch
    }




    //void Start()
    //{
    //    MacroInput.SetMacroManager()
    //    MacroInput.PlayMacro("DefaultMacro");

    //}


    public static void Play(string name) 
    {
        CheckForInstance();
        MacroInput.PlayMacro(name);
    }


    public static T Load<T>(string name) where T : I_Macro
    {
        CheckForInstance();
       return  MacroInput.Load<T>(name);
    }

   
    public static void Save<T>(T macro) where T : I_Macro
    {
        CheckForInstance();
        MacroInput.Save<T>(macro);
    }

    private static void CheckForInstance()
    {
        MacroInputMono i = Instance;
    }


}
